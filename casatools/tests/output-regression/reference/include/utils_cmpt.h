#ifndef _UTILS_XML_UTILS_CMPT_
#define _UTILS_XML_UTILS_CMPT_
/******************** generated by xml-casa (v2) from utils.xml *********************
********************* e9c8cd5c2cb5516958a1fbad2ce89127 *****************************/

#include <vector>
#include <string>
#include <complex>
#include <stdcasa/record.h>
#include <casaswig_types.h>
#include <casa/Exceptions/Error.h>
#include <utils_forward.h>


using namespace std;

namespace casac {

  class  utils  {
    public:

      utils();
      string getrc(const string& _rcvar=string(""));
      bool removetable(const std::vector<std::string>& _tablenames=std::vector<std::string>({}));
      record* tableinfo(const string& _tablename=string(""));
      std::vector<std::string> lockedtables();
      record* hostinfo();
      string c_exception();
      void c_exception_clear();
      string _crash_reporter_initialize(const string& _crashDumpDirectory=string(""), const string& _crashDumpPosterApplication=string(""), const string& _crashPostingUrl=string(""), const string& _logFile=string(""));
      bool _trigger_segfault(long _faultType=long(0));
      double tryit(const record& _input=initialize_record(""));
      long maxint();
      long minint();
      long maxlong();
      long minlong();
      bool initialize(const string& _python_path=string(""), const string& _distro_data_path=string(""), const std::vector<std::string>& _default_path=std::vector<std::string>({}));
      std::vector<std::string> defaultpath();
      bool setpath(const std::vector<std::string>& _dirs=std::vector<std::string>({}));
      std::vector<std::string> getpath();
      void clearpath();
      string resolve(const string& _path=string(""));
      record* registry();
      record* services();
      void shutdown();
      string getpython();
      std::vector<long> version();
      string version_variant();
      string version_desc();
      string version_info();
      string version_string();
      bool compare_version(const string& _comparitor=string(""), const std::vector<long>& _vec=std::vector<long>({}));
      std::vector<long> toolversion();
      string toolversion_string();

        ~utils( );

    private:

#include <utils_private.h>


      // --- declarations of static parameter defaults ---
    public:

  };

}

#endif
