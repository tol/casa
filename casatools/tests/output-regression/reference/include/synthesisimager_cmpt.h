#ifndef _SYNTHESISIMAGER_XML_SYNTHESISIMAGER_CMPT_
#define _SYNTHESISIMAGER_XML_SYNTHESISIMAGER_CMPT_
/******************** generated by xml-casa (v2) from synthesisimager.xml ***********
********************* af4edd88bbdaebe8512b529d877b1285 *****************************/

#include <vector>
#include <string>
#include <complex>
#include <stdcasa/record.h>
#include <casaswig_types.h>
#include <casa/Exceptions/Error.h>
#include <synthesisimager_forward.h>

#include <synthesisimstore_cmpt.h>


using namespace std;

namespace casac {

  class  synthesisimager  {
    public:

      synthesisimager();
      bool selectdata(const record& _selpars=initialize_record(""));
      record* tuneselectdata();
      bool defineimage(const record& _impars=initialize_record(""), const record& _gridpars=initialize_record(""));
      bool setdata(const string& _msname=string(""), const string& _spw=string(""), const string& _freqbeg=string(""), const string& _freqend=string(""), const string& _freqframe=string("LSRK"), const string& _field=string(""), const string& _antenna=string(""), const string& _timestr=string(""), const string& _scan=string(""), const string& _obs=string(""), const string& _state=string(""), const string& _uvdist=string(""), const string& _taql=string(""), bool _usescratch=bool(false), bool _readonly=bool(false), bool _incrmodel=bool(false));
      bool setimage(const string& _imagename=string(""), long _nx=long(128), long _ny=long(-1), const variant& _cellx=variant( ), const variant& _celly=variant( ), const string& _stokes=string("I"), const variant& _phasecenter=variant( ), long _nchan=long(-1), const variant& _freqstart=variant( ), const variant& _freqstep=variant( ), const variant& _restfreq=variant( ), long _facets=long(1), const string& _ftmachine=string("gridft"), long _ntaylorterms=long(1), const variant& _reffreq=variant( ), const string& _projection=string("SIN"), const variant& _distance=variant( ), const string& _freqframe=string("LSRK"), bool _tracksource=bool(false), const variant& _trackdir=variant( ), bool _overwrite=bool(true), float _padding=float(1.0), bool _useautocorr=bool(false), bool _usedoubleprec=bool(true), long _wprojplanes=long(1), const string& _convfunc=string("SF"), const string& _startmodel=string(""), bool _aterm=bool(true), bool _psterm=bool(true), bool _mterm=bool(false), bool _wbawp=bool(true), const string& _cfcache=string(""), bool _usepointing=bool(false), const variant& _pointingoffsetsigdev=variant( ), bool _dopbcorr=bool(true), bool _conjbeams=bool(false), float _computepastep=float(360.0), float _rotatepastep=float(5.0));
      bool setweighting(const string& _type=string("natural"), const string& _rmode=string("norm"), const variant& _noise=variant( ), double _robust=double(0.0), const variant& _fieldofview=variant( ), long _npixels=long(0), bool _multifield=bool(false), bool _usecubebriggs=bool(false), const std::vector<std::string>& _uvtaper=std::vector<std::string>({}));
      bool makepsf();
      record* apparentsens();
      bool predictmodel();
      bool drygridding(const std::vector<std::string>& _cflist=std::vector<std::string>({""}));
      bool fillcfcache(const std::vector<std::string>& _cflist=std::vector<std::string>({""}), const string& _ftmname=string(""), const string& _cfcpath=string(""), bool _pstermon=bool(
	false
      ), bool _atermon=bool(
	true
      ), bool _conjbeams=bool(
	false
      ));
      bool reloadcfcache();
      bool executemajorcycle(const record& _controls=initialize_record(""));
      bool makepb();
      bool makesdimage();
      bool makesdpsf();
      bool makeimage(const string& _type=string("observed"), const string& _image=string(""), const string& _compleximage=string(""), long _model=long(0));
      bool unlockimages(long _imagefieldid=long(0));
      variant* estimatememory();
      casac::synthesisimstore* getimstore(long _id=long(0));
      record* getcsys();
      long updatenchan();
      string getweightdensity();
      bool setweightdensity(const string& _type=string(""));
      bool done();

        ~synthesisimager( );

    private:

#include <synthesisimager_private.h>


      // --- declarations of static parameter defaults ---
    public:

  };

}

#endif
