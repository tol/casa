#ifndef _ATMOSPHERE_XML_ATMOSPHERE_CMPT_
#define _ATMOSPHERE_XML_ATMOSPHERE_CMPT_
/******************** generated by xml-casa (v2) from atmosphere.xml ****************
********************* e79b0bbe7303e5be53ed0d1656b65f07 *****************************/

#include <vector>
#include <string>
#include <complex>
#include <stdcasa/record.h>
#include <casaswig_types.h>
#include <casa/Exceptions/Error.h>
#include <atmosphere_forward.h>


using namespace std;

namespace casac {

  class  atmosphere  {
    public:

      atmosphere();
      bool close();
      bool done();
      string getAtmVersion();
      std::vector<std::string> listAtmosphereTypes();
      string initAtmProfile(const Quantity& _altitude=Quantity(std::vector<double>(1,5000.),"m"), const Quantity& _temperature=Quantity(std::vector<double>(1,270.0),"K"), const Quantity& _pressure=Quantity(std::vector<double>(1,560.0),"mbar"), const Quantity& _maxAltitude=Quantity(std::vector<double>(1,48.0),"km"), double _humidity=double(20.0), const Quantity& _dTem_dh=Quantity(std::vector<double>(1,-5.6),"K/km"), const Quantity& _dP=Quantity(std::vector<double>(1,10.0),"mbar"), double _dPm=double(1.2), const Quantity& _h0=Quantity(std::vector<double>(1,2.0),"km"), long _atmType=long(1), const std::vector<double>& _layerBoundaries=std::vector<double>({}), const std::vector<double>& _layerTemperature=std::vector<double>({}));
      string updateAtmProfile(const Quantity& _altitude=Quantity(std::vector<double>(1,5000.),"m"), const Quantity& _temperature=Quantity(std::vector<double>(1,270.0),"K"), const Quantity& _pressure=Quantity(std::vector<double>(1,560.0),"mbar"), double _humidity=double(20.0), const Quantity& _dTem_dh=Quantity(std::vector<double>(1,-5.6),"K/km"), const Quantity& _h0=Quantity(std::vector<double>(1,2.0),"km"));
      string getBasicAtmParms(Quantity& _altitude=_altitude_getBasicAtmParms, Quantity& _temperature=_temperature_getBasicAtmParms, Quantity& _pressure=_pressure_getBasicAtmParms, Quantity& _maxAltitude=_maxAltitude_getBasicAtmParms, double& _humidity=_humidity_getBasicAtmParms, Quantity& _dTem_dh=_dTem_dh_getBasicAtmParms, Quantity& _dP=_dP_getBasicAtmParms, double& _dPm=_dPm_getBasicAtmParms, Quantity& _h0=_h0_getBasicAtmParms, string& _atmType=_atmType_getBasicAtmParms);
      long getNumLayers();
      Quantity getGroundWH2O();
      string getProfile(Quantity& _thickness=_thickness_getProfile, Quantity& _temperature=_temperature_getProfile, Quantity& _watermassdensity=_watermassdensity_getProfile, Quantity& _water=_water_getProfile, Quantity& _pressure=_pressure_getProfile, Quantity& _O3=_O3_getProfile, Quantity& _CO=_CO_getProfile, Quantity& _N2O=_N2O_getProfile);
      long initSpectralWindow(long _nbands=long(1), const Quantity& _fCenter=Quantity(std::vector<double>(1,90),"GHz"), const Quantity& _fWidth=Quantity(std::vector<double>(1,0.64),"GHz"), const Quantity& _fRes=Quantity(std::vector<double>(1,0.0),"GHz"));
      long addSpectralWindow(const Quantity& _fCenter=Quantity(std::vector<double>(1,350),"GHz"), const Quantity& _fWidth=Quantity(std::vector<double>(1,0.008),"GHz"), const Quantity& _fRes=Quantity(std::vector<double>(1,0.002),"GHz"));
      long getNumSpectralWindows();
      long getNumChan(long _spwid=long(0));
      long getRefChan(long _spwid=long(0));
      Quantity getRefFreq(long _spwid=long(0));
      Quantity getChanSep(long _spwid=long(0));
      Quantity getChanFreq(long _chanNum=long(0), long _spwid=long(0));
      Quantity getSpectralWindow(long _spwid=long(0));
      double getChanNum(const Quantity& _freq=Quantity(std::vector<double>(1,0.0),"GHz"), long _spwid=long(0));
      Quantity getBandwidth(long _spwid=long(0));
      Quantity getMinFreq(long _spwid=long(0));
      Quantity getMaxFreq(long _spwid=long(0));
      double getDryOpacity(long _nc=long(-1), long _spwid=long(0));
      double getDryContOpacity(long _nc=long(-1), long _spwid=long(0));
      double getO2LinesOpacity(long _nc=long(-1), long _spwid=long(0));
      double getO3LinesOpacity(long _nc=long(-1), long _spwid=long(0));
      double getCOLinesOpacity(long _nc=long(-1), long _spwid=long(0));
      double getN2OLinesOpacity(long _nc=long(-1), long _spwid=long(0));
      Quantity getWetOpacity(long _nc=long(-1), long _spwid=long(0));
      double getH2OLinesOpacity(long _nc=long(-1), long _spwid=long(0));
      double getH2OContOpacity(long _nc=long(-1), long _spwid=long(0));
      long getDryOpacitySpec(long _spwid=long(0), std::vector<double>& _dryOpacity=_dryOpacity_getDryOpacitySpec);
      long getWetOpacitySpec(long _spwid=long(0), Quantity& _wetOpacity=_wetOpacity_getWetOpacitySpec);
      Quantity getDispersivePhaseDelay(long _nc=long(-1), long _spwid=long(0));
      Quantity getDispersiveWetPhaseDelay(long _nc=long(-1), long _spwid=long(0));
      Quantity getNonDispersiveWetPhaseDelay(long _nc=long(-1), long _spwid=long(0));
      Quantity getNonDispersiveDryPhaseDelay(long _nc=long(-1), long _spwid=long(0));
      Quantity getNonDispersivePhaseDelay(long _nc=long(-1), long _spwid=long(0));
      Quantity getDispersivePathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getDispersiveWetPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getNonDispersiveWetPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getNonDispersiveDryPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getO2LinesPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getO3LinesPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getCOLinesPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getN2OLinesPathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getNonDispersivePathLength(long _nc=long(-1), long _spwid=long(0));
      Quantity getAbsH2OLines(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsH2OCont(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsO2Lines(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsDryCont(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsO3Lines(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsCOLines(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsN2OLines(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsTotalDry(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      Quantity getAbsTotalWet(long _nl=long(0L), long _nf=long(0), long _spwid=long(0));
      bool setUserWH2O(const Quantity& _wh2o=Quantity(std::vector<double>(1,0.0),"mm"));
      Quantity getUserWH2O();
      bool setAirMass(double _airmass=double(0.0));
      double getAirMass();
      bool setSkyBackgroundTemperature(const Quantity& _tbgr=Quantity(std::vector<double>(1,2.73),"K"));
      Quantity getSkyBackgroundTemperature();
      Quantity getAverageTebbSky(long _spwid=long(0), const Quantity& _wh2o=Quantity(std::vector<double>(1,-1),"mm"));
      Quantity getTebbSky(long _nc=long(-1), long _spwid=long(0), const Quantity& _wh2o=Quantity(std::vector<double>(1,-1),"mm"));
      long getTebbSkySpec(long _spwid=long(0), const Quantity& _wh2o=Quantity(std::vector<double>(1,-1),"mm"), Quantity& _tebbSky=_tebbSky_getTebbSkySpec);
      Quantity getAverageTrjSky(long _spwid=long(0), const Quantity& _wh2o=Quantity(std::vector<double>(1,-1),"mm"));
      Quantity getTrjSky(long _nc=long(-1), long _spwid=long(0), const Quantity& _wh2o=Quantity(std::vector<double>(1,-1),"mm"));
      long getTrjSkySpec(long _spwid=long(0), const Quantity& _wh2o=Quantity(std::vector<double>(1,-1),"mm"), Quantity& _trjSky=_trjSky_getTrjSkySpec);

        ~atmosphere( );

    private:

#include <atmosphere_private.h>


      // --- declarations of static parameter defaults ---
    public:
      static Quantity _altitude_getBasicAtmParms;
      static Quantity _temperature_getBasicAtmParms;
      static Quantity _pressure_getBasicAtmParms;
      static Quantity _maxAltitude_getBasicAtmParms;
      static double _humidity_getBasicAtmParms;
      static Quantity _dTem_dh_getBasicAtmParms;
      static Quantity _dP_getBasicAtmParms;
      static double _dPm_getBasicAtmParms;
      static Quantity _h0_getBasicAtmParms;
      static string _atmType_getBasicAtmParms;
      static Quantity _thickness_getProfile;
      static Quantity _temperature_getProfile;
      static Quantity _watermassdensity_getProfile;
      static Quantity _water_getProfile;
      static Quantity _pressure_getProfile;
      static Quantity _O3_getProfile;
      static Quantity _CO_getProfile;
      static Quantity _N2O_getProfile;
      static std::vector<double> _dryOpacity_getDryOpacitySpec;
      static Quantity _wetOpacity_getWetOpacitySpec;
      static Quantity _tebbSky_getTebbSkySpec;
      static Quantity _trjSky_getTrjSkySpec;
  };

}

#endif
